censored = ['stupid', 'moron', 'ugly']
filename = raw_input('Enter a filename to censor:')
r = open(filename, 'r')
w = open(filename + '.censored', 'w')


for word in r:
	for i in range(0, len(censored)):
		word = word.replace(censored[i], "*" * len(censored[i]))
	w.write(word + "\n")

r.close()
w.close()