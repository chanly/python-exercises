
class Student(object):
	"""docstring for Student"""
	def __init__(self):
		self.student = ""
		self.subjects = []
	def setName(self, name):
		self.name = name
	def getName(self):
		return self.name
	def addSubjects(self, subjects):
		self.subjects.append(subjects)
	def getSubjects(self):
		return self.subjects
	
class Teacher(object):
	def __init__(self):
		self.name = ""
		self.subjects = []
	def setName(self, name):
		self.name = name
	def getName(self):
		return self.name
	def addSubjects(self, subjects):
		self.subjects.append(subjects)
	def getSubjects(self):
		return self.subjects

class Subject(object):
	def __init__(self):
		self.subjects = {"Math": "None"}

	def addSub(self, sub, teachers):
		self.subjects.setdefault(sub, teachers)
		self.subjects[sub] = teachers
	def getSub(self):
		return self.subjects

t = Teacher()
t.setName("Ma'am Ruiz")

r = Subject()
r.addSub("Math", t)
print r.getSub()