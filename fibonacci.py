def secret_function(n): #fibonacci numbers
	if n < 1:
		raise ValueError('positive only')
	if n == 1:
		return [1]
	result = [1,1]

	for i in range(n-2):
		result.append(result[-2] + result[-1])
	return result

# factorial function
def another_secret(n):
	result = 1
	for i in range(1, n+1): #from 1 - 10
		result = result * i # 1 * 1 = 1 * 2 = 2 * 3 = 6 and so on and so forth
	return result

print secret_function(6)
print another_secret(10)