class Student(object):
	def __init__(self):
		self.name = ""
		self.subjects = []

	def setName(self, name):
		self.name = name

	def getname(self):
		return self.name

	def addSubjects(self, subjects):
		self.subjects.append(subjects)

	def getSubjects(self):
		return self.subjects


class Subjects(object):
	
	def __init__(self, name):
		self.name = name
		self.teacher = Teacher("Sheila", [])

	def getName(self):
		return self.name

	def setTeacher(self, teacher):
		self.teacher = teacher

	def getTeacher(self):
		return self.teacher


class Teacher(object):
	def __init__(self, name, subjects):
		self.name = name
		self.subjects = subjects
		for sub in subjects:
			sub.setTeacher(self)
	def setName(self, name):
		self.name = name
	def getName(self):
		return self.name
	def addSubjects(self, subjects):
		self.subjects.extend(subjects)

database = Subjects("Database Systems")
comNet = Subjects("Computer Networks")
dsalgo = Subjects("Data Structures and Algorithms")
infosec = Subjects("Information Security")
techno = Subjects("Technopreneurship")

pat = Teacher("Pat", [database, comNet, techno])
sheila = Teacher("Sheila", [dsalgo, infosec])

s2 = Student()
s2.setName("Chanly")
s2.addSubjects(database)
s2.addSubjects(comNet)
s2.addSubjects(dsalgo)
s2.addSubjects(infosec)
s2.addSubjects(techno)

keks = Student()
keks.setName("Keks")
keks.addSubjects(techno)
keks.addSubjects(infosec)


for sub in s2.getSubjects():
	print sub.getName()

print database.getTeacher().getName()
